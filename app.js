'use strict';

const https = require('https');
const http = require('http');
const  path    = require("path");
const fs = require('fs');

const companyName = 'flexsuccess';
const api = 'flexsuccess.erpsync.co';
const APIKEY= '02135ba0-9593-43e9-882b-1f1b032ac1f8';

const PORT = 3010;
//const SSLPORT = 443;
var ROOT_DIR = path.resolve(__dirname, '.');
var config = {root : ROOT_DIR, useLiquid: true, company : companyName, api : api , apiKey : APIKEY, storeId : 31, version: '1.0.1', appVersion: 'v2' };
//load the MVC
let options = {
    key: fs.readFileSync(__dirname + '/ssl/cert.key'),
    cert: fs.readFileSync(__dirname + '/ssl/cert.crt')
};

let app = require('./core/server')(config);
//http.createServer(app).listen(PORT);
// Create an HTTPS service identical to the HTTP service.
https.createServer(options, app).listen(PORT);
console.log('Running on http://localhost ON port ' + PORT  );

