"use strict"; // Start of use strict

// 7. google map
function gMap () {
	if ($('.google-map').length) {
        $('.google-map').each(function () {
        	// getting options from html 
        	var mapName = $(this).attr('id');
        	var mapLat = $(this).data('map-lat');
        	var mapLng = $(this).data('map-lng');
        	var iconPath = $(this).data('icon-path');
        	var mapZoom = $(this).data('map-zoom');
        	var mapTitle = $(this).data('map-title');
			var styles = [{"featureType": "administrative", "elementType": "labels.text.fill", "stylers": [{"color": "#444444"} ] }, {"featureType": "landscape", "elementType": "all", "stylers": [{"color": "#f2f2f2"} ] }, {"featureType": "poi", "elementType": "all", "stylers": [{"visibility": "off"} ] }, {"featureType": "road", "elementType": "all", "stylers": [{"saturation": -100 }, {"lightness": 45 } ] }, {"featureType": "road.highway", "elementType": "all", "stylers": [{"visibility": "simplified"} ] }, {"featureType": "road.arterial", "elementType": "labels.icon", "stylers": [{"visibility": "off"} ] }, {"featureType": "transit", "elementType": "all", "stylers": [{"visibility": "off"} ] }, {"featureType": "water", "elementType": "all", "stylers": [{"color": "#BF1E2D"}, {"visibility": "on"} ] } ];

			

        	
        	// if zoom not defined the zoom value will be 15;
        //	if (!mapZoom) {
        		var mapZoom = 2;
        	//};
        	// init map
        	var map;
            map = new GMaps({
                div: '#'+mapName,
                scrollwheel: false,
                lat: mapLat,
                lng: mapLng,
                styles: styles,
                zoom: mapZoom
            });
            // if icon path setted then show marker
            if(iconPath) {
        		map.addMarker({
	            	icon: iconPath,
	                lat: mapLat,
	                lng: mapLng,
	                title: mapTitle
	            });
        		
	            	map.addMarker({
	            	icon: iconPath,
	                lat: -33.8688197,
	                lng: 151.2092955,
	                title: mapTitle
	            });
	              	map.addMarker({
	            	icon: iconPath,
	                lat: -27.4697707,
	                lng: 153.0251235,
	                title: mapTitle
	            });
        		map.addMarker({
	            	icon: iconPath,
	                lat: -28.016667,
	                lng: 153.4,
	                title: mapTitle
	            });
	            	map.addMarker({
	            	icon: iconPath,
	                lat: 22.2783151,
	                lng: 114.174695,
	                title: mapTitle
	            });
	            	map.addMarker({
	            	icon: iconPath,
	                lat: -33.8688197,
	                lng: 151.2092955,
	                title: mapTitle
	            });
	              	map.addMarker({
	            	icon: iconPath,
	                lat: -26.65,
	                lng: 153.066667,
	                title: mapTitle
	            });
	             	map.addMarker({
	            	icon: iconPath,
	                lat: -19.2589635,
	                lng: 146.8169483,
	                title: mapTitle
	            });
	             	map.addMarker({
	            	icon: iconPath,
	                lat: -19.2589635,
	                lng: 146.8169483,
	                title: mapTitle
	            });
	            	map.addMarker({
	            	icon: iconPath,
	                lat: 26.93186516,
	                lng: -80.43708801,
	                title: mapTitle
	            });
	            	
	            	
        	}
        });  
	};
}



// instance of fuction while Document ready event	
jQuery(document).on('ready', function () {
	(function ($) {
		gMap();
	})(jQuery);
});