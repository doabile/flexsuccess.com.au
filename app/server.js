'use strict';

module.exports = function (config) {
    const express = require('express');
    const APPVERSION = config.appVersion ? config.appVersion : 'v2';
   // process.env.APPVERSION = APPVERSION;
    process.env['APPVERSION'] = APPVERSION;
    //var controller = require('../core/' + APPVERSION + '/Controller');
   // var erpsync = new controller(config);
// App
    const app = express();
    app.use(express.static(config.root + '/public'));
    app.disable('x-powered-by');
  
    return {app: app};
}
