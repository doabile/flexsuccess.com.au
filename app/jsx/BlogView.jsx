import React from 'react';
import {render} from 'react-dom';

class BlogView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {blog : {author:{}, dates: {}, category: []}};
    }

    componentDidMount() {
        let alias = location.pathname.split('/')[2];
        if(alias ) {
        $.get("/api/cms/blogs/alias", {limit : 1, alias: alias}, (result) => {
           if(result.success === true ) {
              this.setState({blog: result.data});
             $('.dynamic-title').text(result.data.title);
         }else {
             $('.dynamic-title').text('Blog Not Found');
         }
        });
    }
    }
    render() {
        let tags = [];
        if(this.state.blog.tags) {
            tags = this.state.blog.tags;
        }
        let category = '';
        if(this.state.blog.category.length > 0 ) {
            category = this.state.blog.category[0].name;
        }
        return (

               <div className="sidebar-page-content section-padding pb0">
                    <div className="section-padding pt0 white-bg">
                        <div className="single-blog-post">
                            <div className="img-holder">
                                <img src={ this.state.blog.image_path } alt={ this.state.blog.title } />

                            </div>
                            <div className="content">
                                <ul className="meta">
                                    <li><a href=""><i className="fa fa-calendar"></i>{ this.state.blog.dates.current  }</a></li>
                                    <li><a href=""><i className="fa fa-user"></i>{ this.state.blog.author.firstname }</a></li>
                                     <li><a href=""><i className="fa fa-object-group"></i>{ category }</a></li>
                                    { tags.map( (tag, idx) =>  <li key={ idx } ><a href={'/blogs/tags/' + tag.text }><i className="fa fa-tag"></i>{ tag.text }</a></li> )} 
                                </ul>
                                <h3>{ this.state.blog.title }</h3>
                           
                                <p dangerouslySetInnerHTML={{__html: this.state.blog.content }} ></p>
                           
                                <div className="share-box">
                                    <label>Did You Like This Post? Share it :</label>
                                    <ul className="social-icons">
                                        <li><a href="https://www.facebook.com/successwithFS"><i className="fa fa-facebook"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                      
        )
    }
    
}


export default BlogView;
if($('#blog-view').length > 0 ) {
render( <BlogView />, document.getElementById('blog-view'));
}

