import React from 'react';
import {render}
from 'react-dom';
class PremiumSubscription extends React.Component {
    constructor(props) {
        super(props);
        this.state = {product: {}};
    }

    componentDidMount() {
        $.get("/api/catalog/products/get", {seqno: 2063, limit: 1}, (res) => {
            this.setState({product: res.data});
        });
    }
    render() {
        return (
                <div className="row">
                    <div className="container">
                        <div className="col-md-8 col-sm-7">
                        <h3>{ this.state.product.title }</h3>
                        <p dangerouslySetInnerHTML={{__html: this.state.product.content }}></p></div>
                        <div className="col-md-4 col-sm-5"><div className="thumbnail"><img src={ this.state.product.base_image } alt={ this.state.product.title } /></div></div>
                        <div className=" col-sm-5 col-md-offset-2">    
                                <a href="#" className="button btn btn-danger btn-lg btn-block">{ this.state.product.checkout_btn_text }</a></div>
                      
                    </div>
                </div>
                            );
                }

    }

    export default PremiumSubscription;
    if ($('#premium-subscription').length > 0) {
        render(<PremiumSubscription />, document.getElementById('premium-subscription'));
    }

