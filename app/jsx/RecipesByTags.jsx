import React from 'react';
import {render} from 'react-dom';

class RecipesByTags extends React.Component {
    constructor(props) {
        super(props);
        this.state = {recipes : []};
    }

    componentDidMount() {
       let alias = location.pathname.split('/')[3];
       if(alias) {
        $.get("/api/cms/recipes/bytags", {limit : 30, alias : alias}, (result) => {
            if(result.success === true) {
                this.setState({recipes: result.data});
                $('.dynamic-title').text(result.tag.text);
            }
        });
    }
    }
    render() {
        return (
                
               <div className="single-sidebar post-widget">
                    <ul className="recent-news">
                     { this.state.recipes.map( (recipe, idx) => <RecipeTagList recipe={ recipe } key={ idx } /> )}  
                     </ul>
               </div>
                      
        )
    }
    
}
class RecipeTagList extends React.Component {
   truncate(str, max = 10) {
        if (str) {
            let strArr = str.toString().split(" ");
            if (strArr.length > max) {
                return strArr.splice(0, max).join(" ") + '...';
            }else {
                return str;
            }
        }
        return " ";
    }
    
    render() {
        const recipe = this.props.recipe;
        const content = this.truncate(recipe.intro, 30);
        return (
                       <li>
                    <div className="box search-list">
                      <img alt={ recipe.title } src={ recipe.image_path } />
                       
                    </div>
                   <div className="content">
                   <h3><a  href={ '/recipes/' + recipe.alias }>{ recipe.title }</a></h3>
                         <div dangerouslySetInnerHTML={{__html: content }} ></div>
                         <p className="text-right"> <a className="read-more arrow-push-in-right" href={ '/recipes/' + recipe.alias }><span>read recipe <i className="fa fa-caret-right"></i></span></a></p>
                   
                    </div>
                   
                     
                </li>
                    
                );
    }
}

export default RecipesByTags;
if($('#search-recipes-by-tags').length > 0 ) {
render( <RecipesByTags />, document.getElementById('search-recipes-by-tags'));
}

