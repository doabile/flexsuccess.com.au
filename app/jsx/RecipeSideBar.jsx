import React from 'react';
import {render} from 'react-dom';

class RecipeSideBar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {top: [], blogs: [],  categories: [], tags: []};
    }

    componentDidMount() {
        $.get("/api/cms/recipes/sidebar", {limit: 4, slimit: 5, blogs: true}, (data) => {
            this.setState(data);
        });
        $(function () {
        let blogs = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('text'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            identify: function (obj) {
                return obj;
            },
            remote: {
                url: '/api/cms/recipes/search',
                prepare: (query, settings) => {
                    settings.url += '?term=' + query + '&limit=5';
                    return settings;
                },
                filter: function (result) {
                    if (result.success) {
                        return $.map(result.data.recipes, function (obj) {
                            return obj;
                        });
                    }
                }
            }
        });
        
        let categories = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('text'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            identify: function (obj) {
                return obj;
            },
            remote: {
                url: '/api/cms/recipes/search',
                prepare: (query, settings) => {
                    settings.url += '?term=' + query + '&limit=5';
                    return settings;
                },
                filter: function (result) {
                    if (result.success) {
                        return $.map(result.data.categories, function (obj) {
                            return obj;
                        });
                    }
                }
            }
        });
        
        let tags = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('text'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            identify: function (obj) {
                return obj.text;
            },
            remote: {
                url: '/api/cms/recipes/search',
                prepare: (query, settings) => {
                    settings.url += '?term=' + query + '&limit=5';
                    return settings;
                },
                filter: function (result) {
                    if (result.success) {
                        return $.map(result.data.tags, function (obj) {
                            return obj;
                        });
                    }
                }
            }
        });

        $('#blog-search .typeahead').typeahead({
            highlight: true
        }, {
            name: 'blog-search',
            display: 'text',
            source: categories,
            templates: {
                header: '<h5 class="typeahed-search-headers">Categories</h5>',
                suggestion: Handlebars.compile('<div class="single-result"><strong>{{text}}</div>')
            }
        }, {
            name: 'blog-search',
            display: 'text',
            source: blogs,
            templates: {
                header: '<h5 class="typeahed-search-headers">Recipes</h5>',
                suggestion: Handlebars.compile('<div><div class="blog-search-result"><div class="box"> <img src="{{ image_path }}"></div><div class="content"> {{text}}</div></div></div>')
            }
        }, {
            name: 'blog-search',
            display: 'text',
            source: tags,
            templates: {
                header: '<h5 class="typeahed-search-headers">Tags</h5>',
                suggestion: Handlebars.compile('<div>{{text}}</div>')
            }
        });
        $('#blog-search .typeahead').bind('typeahead:select', function (ev, selected) {
           location.href= '/' +  selected.type + '/' + selected.alias;
        });
    });

    }
    render() {
        return (
                <div className="sidebar-wrapper section-padding sidebar-right-border">
                    <div className="single-sidebar">
                        <div className="title">
                            <h3>Search Keywords</h3>
                        </div>
                       <div className="form-search" id="blog-search"> 
                       <input className="typeahead typeahead-input" type="text" placeholder="search" maxLength="128" autoComplete="ofF" autoCorrect="off" autoCapitalize="off" />
                       <button title="Search" className="button"><span className="fa fa-search"></span></button>
                        </div>

                    </div>
                    <div className="single-sidebar post-widget">
                        <div className="title">
                            <h3>Categories</h3>
                        </div>
                        <ul className="recent-news">
                            { this.state.categories.map((category, idx) => <BlogSideList category={ category } key={ idx } />)}
                
                        </ul>
                    </div>
                    <div className="single-sidebar post-widget">
                        <div className="title">
                            <h3>Recent Blogs </h3>
                        </div>
                        <ul className="recent-news">
                            { this.state.blogs.map((recipe, idx) => <SideRecipeList recipe={ recipe } key={ idx } />)}
                        </ul>
                    </div>
                   
                    <ul className="meta-tags">
                        { this.state.tags.map((tag, idx) => <li key={ idx } ><a href={'/recipes/tags/' + tag.text }><i className="fa fa-tag"></i>{ tag.text }</a></li>)}  
                    </ul>
                </div>

                )
    }

}


    class BlogSideList extends React.Component {
        render() {
            const category = this.props.category;
            let totalBlogs = category.total ? category.total : 0;
            return (
                    <li><a href={ '/recipes/category/' + category.link} >{ category.name }<span className="blog-category-total">{'(' + totalBlogs + ')' }</span></a></li>

                    )
        }
    }


    class SideRecipeList extends React.Component {
        render() {
            const recipe = this.props.recipe;
            return (
                    <li>
                        <div className="box">
                            <img alt={ recipe.title } src={ recipe.image_path } />
                            <div className="overlay">
                                <div className="content-box">
                                    <a href={'/recipes/' + recipe.alias }>
                                        <div className="icon-holder">
                                            <i className="fa fa-rss"></i>
                                        </div>
                                        <div className="text">
                                            <p>VIEW</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div className="content">
                            <a href={ '/recipes/' + recipe.alias }>
                                <p>{ recipe.title }</p>
                            </a>
                        </div>
                    </li>

                    )
        }
    }

    export default RecipeSideBar;
    if ($('#recipe-side-bar').length > 0) {
        render(<RecipeSideBar />, document.getElementById('recipe-side-bar'));
    }

