import React from 'react';
import {render} from 'react-dom';

class Coaches extends React.Component {
    constructor(props) {
        super(props);
        this.state = {staffList : []};
    }

    componentDidMount() {
        $.get("/api/crm/staff/list",{limit: 20 }, (data) => {
            this.setState({staffList: data.data});
        });
    }
    render() {
        return (
                   <span>{ this.state.staffList.map( (staff, idx) => <CoachView staff={ staff } key={ idx } /> )}</span>
                );
    }

}

class ExperienceView extends React.Component {
	
	render() {
    	const experience = this.props.experience;
    	return ( <div> <i className="fa fa-thumbs-o-up"></i><span> { experience.experience_line } </span></div>);
    }
}

class QualificationView extends React.Component {
	render() {
    	const qualification = this.props.qualification;
    	return( <div><i className="fa fa-graduation-cap"></i> <span> { qualification.qualification_line } </span> </div>);
    }
}

class CoachView extends React.Component {
    render() {
        const staff = this.props.staff;
     //   const experiences = JSON.parse( staff.experiences);
       // const qualifications = JSON.parse( staff.qualifications);
        
        return (
                   <div className="row" id={ staff.firstname.trim().toLowerCase() }>
        
            <div className="col-sm-3"> <div className="thumbnail"><img src={ staff.gravatar } alt={ staff.firstname } /></div></div>
            <div className="col-sm-9">
                <div className="single-item">
                    <div className="content">
                        <div className="row">
                            <div className="col-md-7 right-border">
                                <div className="trainer-name">
                                    <div className="name pull-left">
                                        <h3>{ staff.firstname }</h3>
                                    </div>
                                    <div className="icon-holder pull-right">
                                        <i className={ staff.custom } ></i>
                                    </div>
                                </div>
                                <p><b>Coaching Specialisation:</b></p>
                                <div>
                                { staff.experiences.map( (experience, idx) => <ExperienceView experience={ experience } key={ idx } /> )}
                                  
                                </div>
                                <div className="social-links">
                                    <a href="/contact"><img src="/img/enquire.png" style={{ height : '40px'}} /></a>

                                </div>
                            </div> 
                            <div className="col-md-5">   
                                <p><b>Education:</b></p>
                                <div>
                                { staff.qualifications.map( (qualification, idx) => <QualificationView qualification={ qualification } key={ idx } /> )}
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
                );
    }
}
export default Coaches;
if($('#coaches').length > 0 ) {
	render( <Coaches />, document.getElementById('coaches'));
}
