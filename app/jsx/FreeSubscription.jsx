import React from 'react';
import { render }
    from 'react-dom';
class FreeSubscription extends React.Component {
    constructor(props) {
        super(props);
        this.state = { product: {} };
    }

    componentDidMount() {
        $.get("/api/catalog/products/get", { seqno: 2062, limit: 1 }, (res) => {
            this.setState({ product: res.data });
        });
    }
    render() {
        return (
            <div className="row">
                <div className="container">
                    <div className="col-md-8 col-sm-7">
                        <h3>{this.state.product.title}</h3>
                        <p dangerouslySetInnerHTML={{ __html: this.state.product.content }}></p>
                        <div className="col-md-10 col-md-offset-1">
                            <form id="subscribe-frm" data-source="Services - Newsletter">
                                <div className="form-group">
                                    <label htmlFor="fullname">Fullname</label>
                                    <input type="text" className="form-control" data-required="Y" data-validator="fullname" name="name" id="fullname" placeholder="Fullname" />
                                    <span className="help-block"></span>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="email">Email address</label>
                                    <input type="email" className="form-control" name="email" data-validator="email" data-required="Y" id="email" placeholder="Email" />
                                    <span className="help-block"></span>
                                </div>
                                <a href="#" className="button btn btn-danger btn-lg btn-block" id="subscribe-btn" >{this.state.product.checkout_btn_text}</a>
                            </form>
                        </div>
                    </div>
                    <div className="col-md-4 col-sm-5"><div className="thumbnail"><img src={this.state.product.base_image} alt={this.state.product.title} /></div></div>

                </div>
            </div>
        );
    }

}

export default FreeSubscription;
if ($('#free-subscription').length > 0) {
    render(<FreeSubscription />, document.getElementById('free-subscription'));
}

