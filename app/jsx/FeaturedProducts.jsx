import React from 'react';
import { render } from 'react-dom';

class FeaturedProducts extends React.Component {
    constructor(props) {
        super(props);
        this.state = { products: [] };
    }

    componentDidMount() {
        $.get("/api/catalog/products/featured", { limit: 4 }, (res) => {
            this.setState({ products: res.data });
        });
    }
    render() {
        return (
            <div className="col-md-6">
                <div className="section-title text-left">
                    <h1><span>What we offer</span></h1>
                </div>
                <div className="row">
                    {this.state.products.map((product, idx) => <ProductList product={product} key={idx} />)}
                </div>
            </div>
        );
    }

}

class ProductList extends React.Component {
    render() {
        const featured = this.props.product;
        let link = '/' + featured.alias;
        if (featured.checkout_link.length > 0) {
            link = featured.checkout_link;
        }
        return (
            <div className="col-md-6 col-sm-6">
                <div className="single-popular-class">
                    <div className="img-box">
                        <a href={link} className="featured-product"><img alt={featured.name} src={featured.base_image} /></a>
                        <div className="overlay">
                            <div className="content">
                                <div className="box text-center"><h3><a href={link}>View</a></h3></div>
                            </div>
                        </div>
                    </div>
                    <div className="text-box clearfix match-heights">
                        <div className="left-box pull-left">
                            <h4><a href={link}>{featured.title}</a></h4>
                            <Price from_price_text={featured.from_price_text} unitprice={featured.finalprice} />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}


function Price(props) {
    if (props.from_price_text.length > 0) {
        return (<p className="text-center"><span>{props.from_price_text}</span></p>);
    } else {
        return (<p className="text-center"><span > {'$' + parseFloat(props.unitprice).toFixed(2)}</span></p>);
    }
}

export default FeaturedProducts;
if ($('#featured-products').length > 0) {
    render(<FeaturedProducts />, document.getElementById('featured-products'));
}
