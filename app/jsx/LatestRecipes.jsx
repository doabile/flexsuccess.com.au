import React from 'react';
import {render} from 'react-dom';

class LatestRecipes extends React.Component {
    constructor(props) {
        super(props);
        this.state = {recipes : []};
    }

    componentDidMount() {
        $.get("/api/cms/recipes/top", {limit : 4}, (data) => {
            this.setState({recipes: data});
        });
    }
    render() {
        return (
                    
                     <div className="single-footer-widget">
                        <h3 className="title">Recipes</h3>
                        <ul className="recent-news">
                        	{ this.state.recipes.map( (recipe, idx) => <RecipeList recipe={ recipe } key={ idx } /> )}
                        </ul>
                      </div>
                  
                );
    }

}

class RecipeList extends React.Component {
    render() {
        const blog = this.props.recipe;
        return (
        			<li>
                                <div className="box">
                                    <div className="overlay">
                                        <img alt={ blog.title } src={ blog.image_path } style={{width : '75px'}} />
                                        <div className="content-box">
                                         <a href={'/recipes/' +  blog.alias }>
                                            <div className="icon-holder">
                                                <i className="fa fa-rss"></i>
                                            </div>
                                            <div className="text">
                                                <p>VIEW</p>
                                            </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div className="content">
                                    <a href={'/recipes/' +  blog.alias }>
                                        <p>{ blog.title }</p>
                                    </a>
                                </div>
                            </li>   
                );
    }
}
export default LatestRecipes;
if($('#latest-recipes').length > 0 ) {
render( <LatestRecipes />, document.getElementById('latest-recipes'));
}
