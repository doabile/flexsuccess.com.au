import React from 'react';
import {render}
from 'react-dom';
class ReadVideo extends React.Component {
    constructor(props){     
        super(props);
        this.state = {course: { curriculum:[]}};
    }

    componentDidMount() {
        $.get("/api/catalog/courses/get", {seqno: 1505754475331, limit: 1}, (res) => {
            if(! res.data.curriculum) {
                res.data.curriculum = [];
            }
            this.setState({course: res.data});
            
        });
    }
    render() {
        return (
                
                <div className="container">
                <div className="row">
                    <div className="col-sm-4 read-sidebar hidden-xs">
                        <h3 className="read-outline-hdr">Course Outline</h3>
                        <div className="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                             { this.state.course.curriculum.map( (curriculum, idx) => <CurriculumOutline curriculum={ curriculum } id={idx } key={ idx } /> )}
                        </div>
                        <ul className="list-group">
                            <li className="list-group-item"><a href="#" className="btn btn-info btn-block" id="completed">Mark as completed</a></li>
                            <li className="list-group-item"><a href="#" id="quiz">Do the quiz</a></li>
                            <li className="list-group-item"><a href="../../my/learning">Back to learning</a></li>
                            <li className="list-group-item"><a href="../../my">Back to my account</a></li>
                        </ul>
                    </div>
                    <div className="col-sm-8 col-xs-12 read-container">
                        <ol className="breadcrumb read-breadcrumb">
                            <li><a href="/my">My</a></li>
                            <li className="active">{this.state.course.title}</li>
                        </ol>
                        <div className="row course-material">
                            <div className="col-sm-12"><iframe className="read-course-video" src={ this.state.course.video } frameBorder="0" allowFullScreen="allowfullscreen" mozallowFullscreen="mozallowfullscreen" webkitallowfullscreen="webkitallowfullscreen"></iframe></div>
                        </div>
                        <div className="row">
                            <div className="col-sm-9">
                                <h3 className="title color-red">Learning resources</h3>
                            </div>
                            <div className="col-sm-3">
                                <div className="text-right"><a href="#" id="view-modules" className="btn btn-default btn-sm" role="button">View Video</a></div>
                            </div>
                            <div className="col-sm-12 mb30">
                                <ul>
                                    <li>How to lose weight naturally</li>
                                    <li>Macro cheat sheet</li>
                                    <li>Macro cheat sheet</li>
                                    <li>Macro cheat sheet</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  

                            );
                }

    }
class CurriculumOutline extends React.Component {
    render() {
        let curriculum = this.props.curriculum;
        let open = '';
        if(this.props.id == 0) {
            open = 'in'
        }
       return ( <div className="panel panel-default">
                    <div className="panel-heading" role="tab" id={ 'heading-' + this.props.id }>
                        <h4 className="panel-title"><a role="button" data-toggle="collapse" 
                        data-parent="#accordion" href={ '#collapse-' + this.props.id } aria-expanded="true" aria-controls={ 'collapse-' + this.props.id }> { curriculum.title } </a></h4>
                    </div>
                    <div id={ 'collapse-' + this.props.id } className={ 'panel-collapse collapse ' + open } role="tabpanel" aria-labelledby={ 'heading-' + this.props.id }>
                    <div className="panel-body"><span  dangerouslySetInnerHTML={{__html: curriculum.content }}></span> </div>
                    </div>
                </div>)
                
    }
}
    export default ReadVideo;
    if ($('#read-my-courses').length > 0) {
        render(<ReadVideo />, document.getElementById('read-my-courses'));
    }

