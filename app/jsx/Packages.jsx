import React from 'react';
import { render }
    from 'react-dom';
class Packages extends React.Component {
    constructor(props) {
        super(props);
        this.state = { packages: [] };
    }

    componentDidMount() {
        $.get("/api/catalog/products/subscription", { limit: 20, fields: '*' }, (res) => {
            this.setState({ packages: res.data });
        });
    }
    render() {
        return (
            <div className="row">
                {this.state.packages.map((subscription, idx) => <SubscriptionList subscription={subscription} key={idx} />)}
            </div>
        );
    }

}

function FreeSubscription(props) {
    return (
        <h3><span>FREE</span></h3>
    )
}

function PaidSubscription(props) {
    return (
        <h3><span>FROM ${props.price}</span></h3>
    )
}
function Checkoutlink(props) {
    let checkoutText = 'More Details'
    if(props.checkoutText){
        checkoutText = props.checkoutText;
    }
    return (
        <p className="view-subscription"><a className="btn btn-black" href={props.checkoutlink}>
            <span>{checkoutText } <i className="fa fa-caret-right"></i></span></a></p>
    )
}

class SubscriptionList extends React.Component {
    render() {
        const subscription = this.props.subscription;
        const services = subscription.services.slice(0, 5);
        let finalPrice = null;
        if (parseFloat(subscription.sell_price1) === 0) {
            finalPrice = <FreeSubscription  />
        } else {
            finalPrice = <PaidSubscription price={parseFloat(subscription.sell_price1).toFixed(2)} />
        }
        let checkoutLink = (<p className="view-subscription"><a className="btn btn-black btn-block" href={'/' + subscription.alias}><span>More Details <i className="fa fa-caret-right"></i></span></a></p>);
        if (subscription.checkout_link) {
            checkoutLink = <Checkoutlink checkoutlink={subscription.checkout_link} checkoutText={ subscription.checkout_btn_text} />
        }

        return (
            <div className="col-sm-12">
                <div className="single-item">
                    <div className='row'>
                        <div className="col-sm-3">
                            <div className="img-holder">
                                <img alt={subscription.title} src={subscription.base_image} />
                            </div>
                        </div>
                        <div className="col-sm-5 content">
                        <h3>{subscription.title}</h3>
                            <p dangerouslySetInnerHTML={{ __html: subscription.intro }}></p>
                            <div className="top">
                                <div className="content-heading pull-left pricing" style={{ marginLeft: 0 }}>
                                    {finalPrice}
                                </div>
                            </div>

                        </div>
                        <div className="col-sm-4">
                            <div className="package-list">
                                <h4>Package Includes:</h4>
                                <ul>
                                    {services.map((service, idx) => (
                                        <li key={idx} id={idx} ><i className={'fa ' + service.services_class}></i>{service.services_description}</li>
                                    ))}
                                </ul>

                            </div>
                            {checkoutLink}</div>
                    </div>
                </div>
            </div>
        );
    }
}
export default Packages;
if ($('#package-list').length > 0) {
    render(<Packages />, document.getElementById('package-list'));
}

