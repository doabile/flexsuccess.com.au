import React from 'react';
import {render} from 'react-dom';

class RecipesListingIndexByCategory extends React.Component {
    constructor(props) {
        super(props);
        this.state = {categories : []};
    }

    componentDidMount() {
        $.get("/api/cms/recipes/activecategories", {limit : 12}, (result) => {
            this.setState({categories: result});
        });
    }
    render() {
        return (

                <div className="section-padding pt0 white-bg">
                    <div className="row">
                     { this.state.categories.map( (category, idx) => <RecipeCategories category={ category } key={ idx }  index={ idx } /> )}  
                     </div>
                     
                 </div>
                      
        )
    }
    
}
class RecipeCategories extends React.Component {
     
    render() {
        const category = this.props.category;
      
        return (
                <div><div className="col-md-3 col-sm-4 col-xs-12">
        			<div className="single-blog-post">
                               <h3 className={'block-title block-title' + this.props.index }><span>{ category.name }</span></h3>
                                { category.blogs.map( (blog, idx) => <RecipeByCategoryList blog={ blog } key={ idx } /> )}  
                                </div>
                                </div>
                                
                            </div>
                );
    }
}

class RecipeByCategoryList extends React.Component {
     
    render() {
        const blog = this.props.blog;
     
        return (
                <div><div className="col-md-4 col-sm-5 col-xs-12">
        			<div className="single-blog-post">
                                <div className="simple-thumb-category">
                                    <img src={ blog.image_path } alt="" />
                                </div>
                                </div>
                                </div>
                                <div className="col-md-8 col-sm-7 col-xs-12 single-blog-post">
                                
                                <h3 className="widget-post"><a href={ '/recipes/' +  blog.alias }>{ blog.title }</a></h3>
                                <p className="blog-calendar"><i className="fa fa-clock-o"></i> <span>{ blog.dates.current}</span></p>
                            </div> 
                            </div>
                );
    }
}
export default RecipesListingIndexByCategory;
if($('#list-recipe-categories').length > 0 ) {
render( <RecipesListingIndexByCategory />, document.getElementById('list-recipe-categories'));
}

