import React from 'react';
import {render} from 'react-dom';

class Recipes extends React.Component {
    constructor(props) {
        super(props);
        this.state = {recipes : [], packages: []};
    }

    componentDidMount() {
        $.get("/api/cms/recipes/blogproducts", {limit : 6}, (result) => {
             this.setState({recipes: result.blogs, packages : result.subscription});
        });
    }
    render() {
        return (
                  <div className="section-padding pt0 white-bg">
                    <div>
                     { this.state.recipes.map( (recipe, idx) => <RecipeMainList recipe={ recipe } key={ idx } /> )}  
                     </div>
                     <div className="row mb40">                    
                        <h3 className="block-title block-title0"><span>Our packages</span> </h3>
                            { this.state.packages.map((product, idx) => <PackagesSideBar product={ product } key={idx } />)}
                    </div>
                 </div>
        )
    }
    
}

class RecipeMainList extends React.Component {
      truncate(str, max = 10) {
        if (str) {
            let strArr = str.toString().split(" ");
            if (strArr.length > max) {
                return strArr.splice(0, max).join(" ") + '...';
            } else {
                return str;
            }
        }
        return " ";
    }
    render() {
        const recipe = this.props.recipe;
        let tags = [];
        if(recipe.tags) {
            tags = recipe.tags;
        }
        return (
                            <div className="row mb20"><div className="col-md-3 col-sm-4 col-xs-12">
        			<div className="single-blog-post">
                                <div className="simple-thumb">
                                    <img src={ recipe.image_path } alt="" />
                                   
                                </div>
                                </div>
                                </div>
                                <div className="col-md-9 col-sm-10 col-xs-12 single-blog-post">
                                <div className="content main-content">
                                 <a href={ '/recipes/' +  recipe.alias }><h3>{ recipe.title }</h3></a>
                                    <ul className="meta">
                                        <li><a href="#"><i className="fa fa-calendar"></i>{ recipe.dates.current }</a></li>
                                        <li><a href={ '/recipes/author/' + recipe.author.name } ><i className="fa fa-user"></i>{ recipe.author.name }</a></li>
                                        
                                        { tags.map( (tag, idx) =>  <li key={ idx } ><a href={'/recipes/tags/' + tag.text }><i className="fa fa-tag"></i>{ tag.text }</a></li> )}  
                                    </ul>
                                   
                                    <p  dangerouslySetInnerHTML={{__html: this.truncate(recipe.intro, 29) }}></p>
                                </div>
                            </div> 
                            </div>
                );
    }
}

class PackagesSideBar extends React.Component {

    render() {
        const subscription = this.props.product;
        const services = subscription.services.slice(0, 2);
        let finalPrice = null;
        if (parseFloat(subscription.sell_price1) === 0) {
            finalPrice = <FreeSubscription name ={ subscription.name } />
        } else {
            finalPrice = <PaidSubscription name ={ subscription.name } price={ parseFloat(subscription.sell_price1).toFixed(2) } />
        }
        let checkoutLink = '/' + subscription.alias;
        if (subscription.checkout_link) {
            checkoutLink = subscription.checkout_link;
        }
        return (
                <div className="col-md-3 col-sm-6">
                    <div className="box">
                    <a href={ checkoutLink }> <img alt={ subscription.title } src={ subscription.base_image } /></a>
                    </div>
                    <div className="content">
                        <p>{ subscription.title }</p>
                        <a className="read-more arrow-push-in-right" href={ checkoutLink }><span>View Offer <i className="fa fa-caret-right"></i></span></a>
                
                    </div>
                    </div>

                            );
                }
    }
    

function FreeSubscription(props) {
    return (
            <span>FREE</span>
            )
}

function PaidSubscription(props) {
    return (
            <span>FROM ${ props.price }</span>
            )
}

export default Recipes;
if($('#recipe-index').length > 0 ) {
render( <Recipes />, document.getElementById('recipe-index'));
}

