import React from 'react';
import {render} from 'react-dom';

class Blogs extends React.Component {
    constructor(props) {
        super(props);
        this.state = {blogs : [], packages : []};
    }

    componentDidMount() {
        $.get("/api/cms/blogs/blogproducts", {limit : 5, isactive: 'Y'}, (result) => {
            this.setState({blogs: result.blogs, packages : result.subscription});
        });
    }
    render() {
        return (

                <div className="section-padding pt0 white-bg">
                   
                     { this.state.blogs.map( (blog, idx) => <BlogMainList blog={ blog } key={ idx } /> )}  
              
                     <div className="row mb40">                    
                        <h3 className="block-title block-title0"><span>Our packages</span> </h3>
                            { this.state.packages.map((product, idx) => <PackagesSideBar product={ product } key={idx } />)}
                    </div>
                 </div>
                      
        )
    }
    
}
class PackagesSideBar extends React.Component {

    render() {
        const subscription = this.props.product;
        const services = subscription.services.slice(0, 2);
        let finalPrice = null;
        if (parseFloat(subscription.sell_price1) === 0) {
            finalPrice = <FreeSubscription name ={ subscription.name } />
        } else {
            finalPrice = <PaidSubscription name ={ subscription.name } price={ parseFloat(subscription.sell_price1).toFixed(2) } />
        }
        let checkoutLink = '/' + subscription.alias;
        if (subscription.checkout_link) {
            checkoutLink = subscription.checkout_link;
        }
        return (
                <div className="col-md-3 col-sm-6">
                    <div className="box">
                    <a href={ checkoutLink }> <img alt={ subscription.title } src={ subscription.base_image } /></a>
                    </div>
                    <div className="content">
                        <p>{ subscription.title }</p>
                        <a className="read-more arrow-push-in-right" href={ checkoutLink }><span>View Offer <i className="fa fa-caret-right"></i></span></a>
                
                    </div>
                    </div>

                            );
                }
    }
    
class BlogMainList extends React.Component {
      truncate(str, max = 10) {
        if (str) {
            let strArr = str.toString().split(" ");
            if (strArr.length > max) {
                return strArr.splice(0, max).join(" ") + '...';
            } else {
                return str;
            }
        }
        return " ";
    }

    render() {
        const blog = this.props.blog;
        let tags = [];
        if(blog.tags) {
            tags = blog.tags;
        }
        return (
                 <div className="row mb20 blog-listing-items"><div className="col-md-3 col-sm-4 col-xs-12">
        			<div className="single-blog-post">
                                <div className="simple-thumb">
                                    <img src={ blog.image_path } alt="" />
                                   
                                </div>
                                </div>
                                </div>
                                <div className="col-md-9 col-sm-10 col-xs-12 single-blog-post">
                                <div className="content main-content">
                                 <a href={ '/blogs/' +  blog.alias }><h3>{ blog.title }</h3></a>
                                    <ul className="meta">
                                        <li><a href="#"><i className="fa fa-calendar"></i>{ blog.dates.current }</a></li>
                                        <li><a href={ '/blogs/author/' + blog.author.firsname } ><i className="fa fa-user"></i>Coach { blog.author.firstname }</a></li>
                                        
                                        { tags.map( (tag, idx) =>  <li key={ idx } ><a href={'/blogs/tags/' + tag.text }><i className="fa fa-tag"></i>{ tag.text }</a></li> )}  
                                    </ul>
                                   
                                    <p  dangerouslySetInnerHTML={{__html: this.truncate(blog.intro, 29) }}></p>
                                </div>
                            </div> 
                            </div>
                );
    }
}


function FreeSubscription(props) {
    return (
            <span>FREE</span>
            )
}

function PaidSubscription(props) {
    return (
            <span>FROM ${ props.price }</span>
            )
}



export default Blogs;
if($('#blog-index').length > 0 ) {
render( <Blogs />, document.getElementById('blog-index'));
}

