import React from 'react';
import {render}
from 'react-dom';
var handler = null;
class LearningProgram extends React.Component {
    constructor(props) {
        super(props);
        this.state = {course: {whats_included:'', what_will_i_learn:'', requirements: '' }, paymentParams : {}, paymentConfig : {}};
        this.handlePayment = this.handlePayment.bind(this);
    }

    componentDidMount() {
        $.get("/api/catalog/courses/alias", {alias: 'online-course', limit : 1 }, (res) => {
            this.setState({course: res.data });
            $(() => {
                WEBSITE.initStripe();
            });
        });
        
    }
    handlePayment(e){
         e.preventDefault();
            let params = {};
                    // Open Checkout with further options:
                    params.description = $(e.target).data('description');
                    params.amount = $(e.target).data('amount');
                    params.seqno = $(e.target).data('seqno');
                    params.amountFormatted = $(e.target).data('amountformatted');
                    params.type = 'course';
                    params.capture = 'Y';
                    params.createAccount = 'Y';
                    params.dr_seqno = 1510271865976;
                    params.hasForm = 'Y';
                    params.view_all = 'Y';
                    params.auto_login = 'Y';
                    params.model = 'Course',
                    params.currency = 'aud';
                    params.disableEmail = 'N';
                    params.hideAddress = 'N'
                    params.plan = '';
                    params.identity = 'stripe';
                    
                    WEBSITE.setStripeParams(params);
                     
                    WEBSITE.stripeHandler.open({
                        amount: params.amount,
                        name: "FLEXSUCCESS",
                        description: params.amountFormatted,
                        shippingAddress:  true,
                        billingAddress: true,
                        alipay: "auto",
                        currency: params.currency
                    });
    }
    render() {
        const includes = '<ul><li>' + this.state.course.whats_included.replace(/\n/g, '<li>') + '</ul>' ;
         const whatYouLearn = '<ul><li>' + this.state.course.what_will_i_learn.replace(/\n/g, '<li>') + '</ul>' ;
         const requirements = '<ul><li>' + this.state.course.requirements.replace(/\n/g, '<li>') + '</ul>' ;
    let product = this.state.product;
    //onClick={this.handlePayment}
    //<h3> ${ parseFloat(this.state.course.sell_price1).toFixed(2)}</h3>
        return (
                 <div className="row">
          <section className="inner-banner pattern-2">
        <div className="container">
        <div className="row">
                    <div className="col-sm-8">
            <h2>{ this.state.course.title }</h2>
            <p>{ this.state.course.description }</p>
            </div>
            </div>
        </div>
    </section>
             <div className="col-sm-12 learning-default-bg">
              <section className="container"  style={{marginBottom:'50px', marginTop:'50px',minHeight:'400px'}}>
                <div className="row">
                    <div className="col-sm-8">
                          <div className="well well-lg">
                                <h3>What will I learn?</h3>
                                <p id="what-you-learn" dangerouslySetInnerHTML={{__html: whatYouLearn }}></p>
                            </div>
                            <h3>Requirements</h3>
                            <p id="what-you-learn" dangerouslySetInnerHTML={{__html: requirements }}></p>
                            <h3>Description</h3>
                            <p dangerouslySetInnerHTML={{__html: this.state.course.content }}></p>
                            <h3>Curriculum For This Course</h3>
                      </div>
                      <div className="col-sm-4">
                      <div className="course-price">
                              <a href="#" className="thumbnail"><img src={ this.state.course.base_image } alt={ this.state.course.title } /> </a>
                              
                               <div className="buy-now">   
                                <button className="btn btn-danger btn-block btn-lg" href="#" role="button" data-amount={ parseFloat(this.state.course.sell_price1 * 100).toFixed(2) } 
                                  data-amountFormatted={ '$' + parseFloat(this.state.course.sell_price1).toFixed(2) } data-description={ this.state.course.title } data-seqno={ this.state.course.seqno} >COMING SOON</button>
                            </div>
                            <p dangerouslySetInnerHTML={{__html:includes}}></p>
                            </div>
                       </div>
                 </div>
               
             </section>
             </div>
               
            </div>
                );
    }

}

class EventList extends React.Component {
    render() {
        const event = this.props.event;
        const product = this.props.product;
        //const material = subscription.services.slice(0,5);
        let key = this.props.id;
       let bgclass = 'learning-program-bg-one';
       if(key % 2 ) {
           bgclass = 'learning-program-bg-two';
       }
        return (
                 
              <div className={ 'col-sm-12 ' + bgclass }>
          <section className="container learning-program-container">
              <div className="row">
                  <div className="col-sm-6">
                  <h2>{event.description }</h2>
                  <p  dangerouslySetInnerHTML={{__html: event.content }}></p>
                  </div>
                  <div className="col-sm-6">
                   <h2>Included Modules:</h2>
                      <ul>
                        { event.materials.map((material, idx) => (
                                        <li key={idx} id={ idx } >{ material.title }</li>
                                    ))}
                      </ul>
                  </div> 
              </div>
            <div className="row"  style={{ marginTop: "30px" }}>
                  <div className="col-sm-8 text-center">
                 	 <h1> Get instant access for only { '$' + parseFloat(product.sell_price1).toFixed(2)} </h1>
                  </div>
                  <div className="col-sm-4 text-center">   
                      <a className="btn btn-success btn-lg signup" href="#" role="button" data-amount={ Math.floor(parseFloat(product.sell_price1) * 100) } 
                  	data-amountFormatted={ '$' + parseFloat(product.sell_price1).toFixed(2) } data-description={ product.description } data-seqno={ product.seqno} onClick={this.handlePayment}>Get Instant access</a>
                  </div>
               </div>    
          </section>
      </div>
                );
    }
}
export default LearningProgram;
if($('#learning-program').length > 0 ) {
    render(<LearningProgram />, document.getElementById('learning-program'));
}

