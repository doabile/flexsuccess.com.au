import React from 'react';
import {render} from 'react-dom';

class BlogsByTags extends React.Component {
    constructor(props) {
        super(props);
        this.state = {blogs : []};
    }

    componentDidMount() {
       let alias = location.pathname.split('/')[3];
       if(alias) {
        $.get("/api/cms/blogs/blogsbytags", {limit : 30, alias : alias}, (result) => {
            if(result.success === true) {
                this.setState({blogs: result.data});
                $('.dynamic-title').text(result.tag.text);
            }
        });
    }
    }
    render() {
        return (
                
               <div className="single-sidebar post-widget">
                    <ul className="recent-news">
                     { this.state.blogs.map( (blog, idx) => <BlogTagList blog={ blog } key={ idx } /> )}  
                     </ul>
               </div>
                      
        )
    }
    
}
class BlogTagList extends React.Component {
   truncate(str, max = 10) {
        if (str) {
            let strArr = str.toString().split(" ");
            if (strArr.length > max) {
                return strArr.splice(0, max).join(" ") + '...';
            }else {
                return str;
            }
        }
        return " ";
    }
    
    render() {
        const blog = this.props.blog;
        const content = this.truncate(blog.intro, 30);
        return (
                       <li>
                    <div className="box search-list">
                      <img alt={ blog.title } src={ blog.image_path } />
                       
                    </div>
                   <div className="content">
                   <h3><a  href={ '/blogs/' + blog.alias }>{ blog.title }</a></h3>
                         <div dangerouslySetInnerHTML={{__html: content }} ></div>
                         <p className="text-right"> <a className="read-more arrow-push-in-right" href={ '/blogs/' + blog.alias }><span>read blog <i className="fa fa-caret-right"></i></span></a></p>
                   
                    </div>
                   
                     
                </li>
                    
                );
    }
}

export default BlogsByTags;
if($('#search-blogs-by-tags').length > 0 ) {
render( <BlogsByTags />, document.getElementById('search-blogs-by-tags'));
}

