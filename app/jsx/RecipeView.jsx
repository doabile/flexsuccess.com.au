import React from 'react';
import {render} from 'react-dom';

class RecipeView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {recipe : {author:{}, dates: {}, category: [], nutritional_info:{}, instructions: [], ingredients : [], method_two:[]}};
    }

    componentDidMount() {
        let alias = location.pathname.split('/')[2];
        if(alias ) {
        $.get("/api/cms/recipes/alias", {limit : 1, alias: alias}, (result) => {
           if(result.success === true ) {
               if(!result.data.hasOwnProperty('ingredients')){
                   result.data.ingredients = [];
               }
               
               if(!result.data.hasOwnProperty('instructions')){
                   result.data.instructions = [];
               }
               
               if(!result.data.hasOwnProperty('nutritional_info')){
                   result.data.nutritional_info = {};
               }
               
              this.setState({recipe: result.data});
             $('.dynamic-title').text(result.data.title);
         }else {
             $('.dynamic-title').text('Recipe Not Found');
         }
        });
    }
    }
    render() {
        let tags = [];
        let instructions = [];
        if(this.state.recipe.tags) {
            tags = this.state.recipe.tags;
        }
    
        if(this.state.recipe.instructions) {
            instructions = this.state.recipe.instructions;
        }
        let category = '';
        if(this.state.recipe.category.length > 0 ) {
            category = this.state.recipe.category[0].name;
        }
      
        return (

               <div className="sidebar-page-content section-padding pb0">
                    <div className="section-padding pt0 white-bg">
                        <div className="single-blog-post">
                        <header className="post-header">
                                        <h1 className="post-title">
                                            {this.state.recipe.title}
                                        </h1>
                                        <p className="simple-share">
                                            <span>by <a href="/authors/dean"><b></b></a></span>
                                            <span><span className="article-date"><i className="fa fa-clock-o"></i> {this.state.recipe.dates.current}</span></span>
                                            <span><i className="fa fa-eye"></i> {this.state.recipe.views } views</span>
                                            <span><a href="#" className="category bgcolor2">Recipes</a></span>

                                        </p>
                                        <figure className="image-overlay">
                                            <img className="blog-main-image" src={this.state.recipe.image_path } alt={this.state.recipe.title} />
                                        </figure>
                                        <div className="nw-recipes-tab clearfix">
                                            <div className="col-sm-2 col-xs-4 nw-recipes-tabs nw-prep">
                                                <p className="text-center">
                                                    <span className="nw-recipe-number">{ this.state.recipe.serves}</span>
                                                    <br /> serves
                                                </p>
                                            </div>
                                            <div className="col-sm-2 col-xs-4 nw-recipes-tabs nw-cook">
                                                <p className="text-center">
                                                    <span className="nw-recipe-number">{this.state.recipe.nutritional_info.proteinContent}g</span>
                                                    <br /> Protein
                                                </p>
                                            </div>
                                            <div className="col-sm-2 col-xs-4 nw-recipes-tabs nw-ingredients">
                                                <p className="text-center">
                                                    <span className="nw-recipe-number">{ this.state.recipe.nutritional_info.fatContent }g</span>
                                                    <br /> Fat
                                                </p>
                                            </div>
                                            <div className="col-sm-2 col-xs-4 nw-recipes-tabs nw-difficulty">
                                                <p className="text-center">
                                                    <span className="nw-recipe-number">{ this.state.recipe.nutritional_info.carbohydrateContent }g</span>
                                                    <br /> Carbs
                                                </p>
                                            </div>
                                            <div className="col-sm-2 col-xs-4 nw-recipes-tabs nw-serves">
                                                <p className="text-center">
                                                    <span className="nw-recipe-number">{ this.state.recipe.nutritional_info.fiberContent }g</span>
                                                    <br /> Fibre
                                                </p>
                                            </div>
                                            <div className="col-sm-2 col-xs-4 nw-recipes-tabs nw-rating">
                                                <p className="text-center">
                                                    <span className="nw-recipe-number">{ this.state.recipe.nutritional_info.calories }</span>
                                                    <br /> kCal
                                                </p>
                                            </div>
                                        </div>
                                    </header>

                                              <div className="post-content clearfix">
                                        <p  dangerouslySetInnerHTML={{__html: this.state.recipe.content }}></p>
                                        <div className="nw-recipes-tab clearfix">
                                            <div className="col-sm-3 col-xs-3 nw-recipes-tabs nw-prep">
                                                <p className="text-center">
                                                    <span className="nw-recipe-number">{ this.state.recipe.to_prep } min.</span>
                                                    <br /> Prep Time
                                                </p>
                                            </div>
                                            <div className="col-sm-3 col-xs-3 nw-recipes-tabs nw-cook">
                                                <p className="text-center">
                                                    <span className="nw-recipe-number">{ this.state.recipe.to_cook } min.</span>
                                                    <br /> Cook Time
                                                </p>
                                            </div>
                                            <div className="col-sm-3 col-xs-3 nw-recipes-tabs nw-ingredients">
                                                <p className="text-center">
                                                    <span className="nw-recipe-number">{ this.state.recipe.ingredients.length }</span>
                                                    <br /> Ingredients
                                                </p>
                                            </div>
                                            <div className="col-sm-3 col-xs-3 nw-recipes-tabs nw-difficulty">
                                                <p className="text-center">
                                                    <span className="nw-recipe-number">{ this.state.recipe.difficulty }</span>
                                                    <br /> Difficulty
                                                </p>
                                            </div>
                                        </div>
                                        <div className="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                            <div className="panel panel-default">
                                                <div className="panel-heading" role="tab" id="headingOne">
                                                    <h4 className="panel-title">
                                                        <a className="toggle-button collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                                            Ingredients <i className="fa fa-plus"></i>
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseOne" className="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style={{height: '0px'}}>
                                                    <div className="panel-body">
                                                         <ul className="ingredients">
                                                                        { this.state.recipe.ingredients.map( (ingredient, idx) =>  <li key={ idx } >{ ingredient.text }</li> )} 
                                                         </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="panel panel-default">
                                                <div className="panel-heading" role="tab" id="headingTwo">
                                                    <h4 className="panel-title">
                                                        <a className="toggle-button collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                            Instructions <i className="fa fa-minus"></i>
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseTwo" className="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo" aria-expanded="false" style={{height: '0px' }}>
                                                    <div className="panel-body"> 
                                                        <ul className="meta">
                                                                        { instructions.map( (info, idx) =>  <li key={ idx }  dangerouslySetInnerHTML={{__html: info.text }} ></li> )} 
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="panel panel-default">
                                                <div className="panel-heading" role="tab" id="headingThree">
                                                    <h4 className="panel-title">
                                                        <a className="toggle-button collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                            Nutritional Info<i className="fa fa-minus"></i>
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseThree" className="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree" aria-expanded="false" style={{height: '0px' }}>
                                                    <div className="panel-body"> 
                                                    <p>&nbsp;</p>
                                                            <table className="table table-condensed">
                                                            <tbody><tr><th>_</th><th className="small">Average Quantity <br/>per Serving</th></tr>
                                                            <HasNutrtionalInfo name="Calories" value={ this.state.recipe.nutritional_info.calories } />
                                                             <HasNutrtionalInfo name="Fats" grams="g" value={ this.state.recipe.nutritional_info.fatContent } />
                                                             <HasNutrtionalInfo name="Carbs"  grams="g" value={ this.state.recipe.nutritional_info.carbohydrateContent } />
                                                             <HasNutrtionalInfo name="Cholesterol"  grams="g" value={ this.state.recipe.nutritional_info.cholesterolContent } />
                                                             <HasNutrtionalInfo name="Fiber"  grams="g" value={ this.state.recipe.nutritional_info.fiberContent } />
                                                             <HasNutrtionalInfo name="Protein"  grams="g" value={ this.state.recipe.nutritional_info.proteinContent } />
                                                             <HasNutrtionalInfo name="Saturated Fat"  grams="g" value={ this.state.recipe.nutritional_info.saturatedFatContent } />
                                                             <HasNutrtionalInfo name="Unsaturated Fat"  grams="g"  value={ this.state.recipe.nutritional_info.unsaturatedFatContent } />
                                                             <HasNutrtionalInfo name="Sodium"  grams="g" value={ this.state.recipe.nutritional_info.sodiumContent } />
                                                             <HasNutrtionalInfo name="Sugar"   grams="g" value={ this.state.recipe.nutritional_info.sugarContent } />
                                                             <HasNutrtionalInfo name="Trans Fat" grams="g"  value={ this.state.recipe.nutritional_info.transFatContent } />
                                                             <HasNutrtionalInfo name="Serving Size" value={ this.state.recipe.nutritional_info.servingSize } />
                                                             </tbody>
                                                            </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div className="share-box">
                                            <label>Did You Like This Recipe? Share it :</label>
                                            <ul className="social-icons">
                                                <li><a href="https://www.facebook.com/successwithFS"><i className="fa fa-facebook"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>              
                        
                        
                        </div>
                    </div>
                </div>
                      
        )
    }
    
}
function HasNutrtionalInfo(props){
   if(props.value) {
      return  (<tr><td>{ props.name }</td> <td>{ props.value } {props.grams }</td></tr>)
   }
   return null;
}

export default RecipeView;
if($('#recipe-view').length > 0 ) {
render( <RecipeView />, document.getElementById('recipe-view'));
}

