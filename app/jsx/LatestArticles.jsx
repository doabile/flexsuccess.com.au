import React from 'react';
import {render} from 'react-dom';

class LatestArticles extends React.Component {
    constructor(props) {
        super(props);
        this.state = {blogs : []};
    }

    componentDidMount() {
        $.get("/api/cms/blogs/top", {limit : 4}, (data) => {
            this.setState({blogs: data});
        });
    }
    render() {
        return (
                    
                     <div className="single-footer-widget">
                        <h3 className="title">Recent News</h3>
                        <ul className="recent-news">
                        	{ this.state.blogs.map( (blog, idx) => <BlogList blog={ blog } key={ idx } /> )}
                        </ul>
                      </div>
                  
                );
    }

}

class BlogList extends React.Component {
    render() {
        const blog = this.props.blog;
        return (
        			<li>
                                <div className="box">
                                    <div className="overlay">
                                        <img alt={ blog.title } src={ blog.image_path } style={{width : '75px' }} />
                                        <div className="content-box">
                                         <a href={'/blogs/' +  blog.alias }>
                                            <div className="icon-holder">
                                                <i className="fa fa-rss"></i>
                                            </div>
                                            <div className="text">
                                                <p>VIEW</p>
                                            </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div className="content">
                                    <a href={'/blogs/' +  blog.alias }>
                                        <p>{ blog.title }</p>
                                    </a>
                                </div>
                            </li>   
                );
    }
}
export default LatestArticles;
if($('#latest-blog').length > 0){
	render( <LatestArticles />, document.getElementById('latest-blog'));
}
