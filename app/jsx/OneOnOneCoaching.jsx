import React from 'react';
import {render}
from 'react-dom';
class OneOnOneCoaching extends React.Component {
    constructor(props) {
        super(props);
        this.state = {product: {services:[], bundle:[]}};
    }

    componentDidMount() {
        $.get("/api/catalog/products/bundle", {seqno: 2051, limit: 1}, (res) => {
            this.setState({product: res});
        });
    }
    render() {
        return (
            <div className="row mt20">
                   
                    { this.state.product.bundle.map( (product, idx) => <BundleItems product={ product } id={ idx } key={ idx } link={ this.state.product.checkout_link } text={ this.state.product.checkout_btn_text } /> )}
                </div>
                            );
                }

    }

    class BundleItems extends React.Component {
        render() {
            let product = this.props.product;
            let link = this.props.link;
            let text = this.props.text;
            let index = parseInt(this.props.id);
            let background = '';
            if(index % 2  === 0 ) {
                background = 'grey-bg';
            }
            return (
            <div className={"row mt20 " + background }>
            <div className="container">
                    <div className="col-md-3 col-sm-4"><div className="thumbnail"><img src={ product.base_image } alt={ product.title } /></div></div>
                        <div className="col-md-5 col-sm-4">
                        <h3>{ product.title }</h3>
                        <p dangerouslySetInnerHTML={{__html: product.content }}></p></div>
                            <div className="col-md-4 col-sm-4">
                                 <div className="package-list">
                         
                                <h4>Package Includes:</h4>
                                <ul className="package-includes">
                                    { product.services.map( (service, idx) => <Services service={ service } key={ idx } /> )}
                                   </ul>
                               
                            </div>
                        </div>
                        <div className=" col-sm-12 text-center">    
                                <a href={ link } className="button btn btn-danger btn-lg">{ text }</a></div>
                      </div>
                    </div>
            )
                    
        }
    }

class Services extends React.Component {
    render() {
       return ( <li><i className="fa fa-check"></i>{ this.props.service.services_description }</li>)
                
    }
}
    export default OneOnOneCoaching;
    if ($('#one-on-one-coaching').length > 0) {
        render(<OneOnOneCoaching />, document.getElementById('one-on-one-coaching'));
    }

