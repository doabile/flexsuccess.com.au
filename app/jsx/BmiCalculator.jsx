import React from 'react';
import {render} from 'react-dom';

class BmiCalculator extends React.Component {
    constructor(props) {
        super(props);
        this.state = {mweight: 0, msession : 1, mage: 0 , mheight: 0,fweight: 0, fsession : 1, fage: 0, fheight:0};
        this.handleClick = this.handleClick.bind(this);
        this.handleFemaleBMI= this.handleFemaleBMI.bind(this);
          this.handleChange = this.handleChange.bind(this);
    }
    handleChange(e) {
        this.setState({
          [e.target.name] : e.target.value
        });
  }
  handleFemaleBMI(e) {
          e.preventDefault();
          if(this.state.fweight && this.state.fsession){
               let bmi = parseInt((this.state.fweight * 10 * 2.2)* this.state.fsession);
                $('#popup-bmi-result').text(bmi);
               $('#bmi-results').modal('show');
               $('#bmi-results #bmi-email').val('');
               $('#bmi-results #bmi-book').off().on('click',()=>{
                  let email = $('#bmi-results #bmi-email').val();
                  let  regex = /^\S+@\S+[\.][0-9a-z]+$/;
                  if(regex.test(email)){
                      this.send(email);
                  }
              });
          }
      }
  handleClick(e) {
          e.preventDefault();
          if(this.state.mweight && this.state.msession){
              //(Weight x 11 x 2.2) x Multiplier
              let bmi = parseInt((this.state.mweight * 11 * 2.2)* this.state.msession);
              $('#popup-bmi-result').text(bmi);
              $('#bmi-results').modal('show');
              $('#bmi-results #bmi-email').val('');
              this. reset();
              $('#bmi-results #bmi-book').off().on('click',()=>{
                  let email = $('#bmi-results #bmi-email').val();
                  let  regex = /^\S+@\S+[\.][0-9a-z]+$/;
                  
                  if(regex.test(email)){
                      this.send(email);
                  }
              });
              
          }
      }
   send(email){
         let  regex = /^\S+@\S+[\.][0-9a-z]+$/;
                   let data = FORMVALIDATOR.setFormId('tdee-frm').checkFormValidation();
                   if(data) {
                        let isFull = data.fullname.toString().split(' ');
                         data.firstname = isFull[0];
                         data.lastname = isFull[1];

                         data.lead_source = 'TDEE Calculations';
                         data.emailTemplate = [{alias: 'tdee-email'}];
                         data.tokenpath = '/media/downloads/private/tdee-explained.pdf';

                         if (!data.hasOwnProperty('company')) {
                             data.company = data.fullname;
                         }
                         if (regex.test(email) && isFull.length > 1) {
                             $.post("/api/crm/leads/create", data, function (data) {
                                 if (data.success === true) {
                                     $('#tdee-content').hide();
                                     $('#thank-you').removeClass('hidden');
                                     $('#bmi-book').hide();
                                     $('#no-thanks').text('Close');
                                 }else {
                                     $('#tdee-error').removeClass('hidden');
                                 }
                             });

                         } else {
                             console.log(email)
                         }
                    }
   }   
 reset(){
    this.setState({mweight: 0, msession : 1, mage: 0 , mheight: 0,fweight: 0, fsession : 1, fage: 0, fheight:0}); 
 }
    
    render() {
        return (
                <div className="col-md-6">
                <div className="section-title text-left">
                    <h1><span>Find Out Your Maintenance Calories</span></h1>
                </div>

                <div className="bmi-tab-box">
                    <div className="bmi-tab-title">
                        <ul role="tablist">
                            <li className="active" data-tab-name="male"><a aria-controls="male" data-toggle="tab" href="#male" role="tab"><i className="fa fa-male"></i> Male</a></li>
                            <li data-tab-name="female"><a aria-controls="female" data-toggle="tab" href="#female" role="tab"><i className="fa fa-female"></i> Female</a></li>
                        </ul>
                    </div>
                    <div className="bmi-tab-content tab-content">
                        <div className="content-box tab-pane fade in active" id="male">
                            <form action="#" className="bmi-calc-form pd10">
                            
                                <div className="row">
                                <div className="col-sm-6">
                                    <div className="form-group">
                                        <label htmlFor="male-bmi-age">Age</label>
                                         <input type="text" className="form-control" id="male-bmi-age" name="mage"  value={ this.state.mage } onChange={this.handleChange} />
                                      </div>
                                </div>
                                <div className="col-sm-6">
                                     <div className="form-group">
                                        <label htmlFor="male-bmi-weight">Weight</label>
                                        <div className="input-group">
                                          <input type="text" className="form-control input-weight" name="mweight" id="male-bmi-weight"  value={ this.state.mweight}  onChange={this.handleChange} />
                                          <div className="input-group-addon">KG</div>
                                        </div>
                                      </div>
                                </div>
                                 <div className="col-sm-6">
                                     <div className="form-group">
                                        <label htmlFor="male-bmi-height">Height</label>
                                        <div className="input-group">
                                          <input type="text" className="form-control input-height-feet" name="mheight" id="male-bmi-height"  onChange={this.handleChange} />
                                          <div className="input-group-addon">CM</div>
                                        </div>
                                      </div>
                                </div>
                                <div className="col-sm-6">
                                    <div className="form-group">
                                        <label htmlFor="male-bmi-sessions">Exercise per week</label>
                                         <select className="form-control input-height-feet"  name="msession"  value={ this.state.msession}  placeholder="Choose session..." id="male-bmi-sessions"  onChange={this.handleChange}>
                                          <option value="1">Once a week</option>
                                          <option value="1.1"> Twice a week</option>
                                          <option value="1.2"> 3 times a week</option>
                                          <option value="1.3">4 times a week</option>
                                          <option value="1.4">5 times a week</option>
                                          <option value="1.5">6 times a week</option>
                                          <option value="1.6"> 7 times a week</option>
                                        </select>
                                      </div>
                                </div>
                                
                                <div className="col-sm-12 mt20 bmi-btn-container text-center">
                                <button  className="thm-btn arrow-push-in-right calculate-bmi mt20" data-gender="male" onClick={this.handleClick}><span>Calculate Male TDEE <i className="fa fa-caret-right"></i></span></button>
                                </div>
                                <div className="col-sm-12 text-center">
                                     <div className="result-box">
                                        <a href="/packages">View Coaching Classes <i className="fa fa-arrow-circle-o-right"></i></a>
                                    </div>
                                </div>
                                </div>
                            </form>
                        </div>
                        <div className="content-box tab-pane fade" id="female">
                            <form action="#" className="bmi-calc-form">
                                <div className="row">
                                <div className="col-sm-6">
                                    <div className="form-group">
                                        <label htmlFor="female-bmi-age">Age</label>
                                         <input type="text" className="form-control" id="female-bmi-age" name="fage"  value={ this.state.fage } onChange={this.handleChange} />
                                      </div>
                                </div>
                                <div className="col-sm-6">
                                     <div className="form-group">
                                        <label htmlFor="female-bmi-weight">Weight</label>
                                        <div className="input-group">
                                          <input type="text" className="form-control input-weight" name="fweight" id="female-bmi-weight"  value={ this.state.fweight}  onChange={this.handleChange} />
                                          <div className="input-group-addon">KG</div>
                                        </div>
                                      </div>
                                </div>
                                 <div className="col-sm-6">
                                     <div className="form-group">
                                        <label htmlFor="female-bmi-height">Height</label>
                                        <div className="input-group">
                                          <input type="text" className="form-control input-height-feet" name="fheight" id="female-bmi-height"  onChange={this.handleChange} />
                                          <div className="input-group-addon">CM</div>
                                        </div>
                                      </div>
                                </div>
                                <div className="col-sm-6">
                                    <div className="form-group">
                                        <label htmlFor="female-bmi-sessions">Exercise per week</label>
                                         <select className="form-control input-height-feet"  name="fsession"  value={ this.state.fsession}  placeholder="Choose session..." id="female-bmi-sessions"  onChange={this.handleChange}>
                                          <option value="1">Once a week</option>
                                          <option value="1.1"> Twice a week</option>
                                          <option value="1.2"> 3 times a week</option>
                                          <option value="1.3">4 times a week</option>
                                          <option value="1.4">5 times a week</option>
                                          <option value="1.5">6 times a week</option>
                                          <option value="1.6"> 7 times a week</option>
                                        </select>
                                      </div>
                                </div>
                                
                                <div className="col-sm-12 mt20 bmi-btn-container text-center">
                                <button  className="thm-btn arrow-push-in-right calculate-bmi mt20" data-gender="female" onClick={this.handleFemaleBMI}><span>Calculate Female TDEE <i className="fa fa-caret-right"></i></span></button>
                                </div>
                                <div className="col-sm-12 text-center">
                                     <div className="result-box">
                                        <a href="/packages">View Coaching Classes <i className="fa fa-arrow-circle-o-right"></i></a>
                                    </div>
                                </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            )
      }
}
            
export default BmiCalculator;
    if($('#bmi-calculator').length > 0 ) {
        render(<BmiCalculator />, document.getElementById('bmi-calculator'));
    }


