import React from 'react';
import {render} from 'react-dom';

class RegisteredCourses extends React.Component {
    constructor(props) {
        super(props);
        this.state = { courses : []};
    }

    componentDidMount() {
        $.get("/api/crm/customers/registrantcourses",{limit: 20 }, (data) => {
            this.setState({courses: data.data});
            if(data.data.length === 0 ) {
                $('#no-registered-courses').removeClass('hidden');
            }
        });
    }
    render() {
        return (
                   <div className="row">
                    { this.state.courses.map( (course, idx) => <CourseView course={ course } key={ idx } /> )}
                </div>
                );
    }

}

class CourseView extends React.Component {
    render() {
        const course = this.props.course;
        return (
                   <div className="col-md-4 col-sm-6 portfolio-item">
                                <figure className="imghvr-flip-horiz"><img src={ '/catalog/' + course.base_image } className="img-responsive" />
                                    <figcaption>
                                        <h3 className="text-center">{ course.title }</h3>
                                        <hr className="style14" />
                                       
                                        <div className="col-xs-12 text-center"><a href={ '/my/course/read/' + course.alias } className="btn btn-success btn-lg text-center bg-black adminguides">Start <i className="fa fa-arrow-right" aria-hidden="true"></i></a></div>
                                    </figcaption>
                                    <div className="flip-caption">Start The Course<i className="fa fa-arrow-right" aria-hidden="true"></i></div>
                                </figure>
                            </div>
                );
    }
}
export default RegisteredCourses;
if($('#my-registered-courses').length > 0 ) {
	render( <RegisteredCourses />, document.getElementById('my-registered-courses'));
}
