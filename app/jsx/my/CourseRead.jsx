import React from 'react';
import {render}
from 'react-dom';
class CourseRead extends React.Component {
    constructor(props) {
        super(props);
        this.state = {course: {curriculum:[]}, current: {}, quiz: {}, module: {curriculum: []}, curriculumIndex : 0, currentModuleIndex: 0 , isText: true, exam: {} };
        this.handleSwitchView = this.handleSwitchView.bind(this);
        this.handleNext = this.handleNext.bind(this);
    }

    handleSwitchView(e){
        e.preventDefault();
        this.setState({isText : !this.state.isText })
    }

    handleNext(e){
        e.preventDefault();
        let current = {};
        
         //if(Array.isArray(this.state.module.curriculum[parseInt(this.state.curriculumIndex) + 1])){
         if(this.state.module.curriculum.length > parseInt(this.state.curriculumIndex) + 1 ){
             current = this.state.module.curriculum[parseInt(this.state.curriculumIndex) + 1];
             if(current.is_quiz === 'Y'){
                      this.loadExam(current);
            }
            this.setState({curriculumIndex: this.state.curriculumIndex + 1, current: current});
        }else if(Array.isArray(this.state.course.curriculum[this.state.currentModuleIndex + 1 ])){
            let module = this.state.course.curriculum[this.state.currentModuleIndex + 1 ];
            current = this.state.module.curriculum[0];
             this.setState({module: module, curriculumIndex: 0, currentModuleIndex: this.state.currentModuleIndex + 1, current: current});
        }else {
            current = this.state.module.curriculum[0];
            this.setState({curriculumIndex: 0, current: current});
        }

    }
    
  loadExam(exam){
       $.get("/api/cms/quiz/get", {seqno: exam.seqno, limit: 1}, (res) => {
            if(Array.isArray(res.data.questions)){
                this.setState({exam : res.data});
                $('#questions-modal').modal('show');
                  $('.verify-answer').off().on('click', (e)=>{
                        let next = $('.card li.active').next();
                       let selectedElement = $('.card li.active input:checked');
                       let answer = selectedElement.data('id');
                       let parent = selectedElement.data('parent');
                       //next-question
                       if(Array.isArray(this.state.exam.questions)){
                           if(this.state.exam.questions[parent].answers[answer].is_correct === 'Y'){
                               selectedElement.parent().parent().parent().parent().css("color", "green" );
                               selectedElement.parent().find('.correct').removeClass('hidden');
                               $('.modal-footer p').addClass('hidden');
                               $('.next-question').removeClass('hidden');
                               $('.next-question').off().on('click', (evt)=>{
                                   
                                   if(parseInt(answer) + 1 === this.state.exam.questions.length ) {
                                       $('.congratulations').removeClass('hidden');
                                       $('finish-question').removeClass('hidden');
                                       $(evt.target).addClass('hidden');
                                       $(e.target).addClass('hidden');
                                   }else {
                                        $('.card li.active').removeClass('active').addClass('hidden');
                                        next.removeClass('hidden').addClass('active');
                                        $(evt.target).addClass('hidden');
                                    }
                               });

                           }else {
                               $('.card li.active input:checked').parent().parent().parent().parent().css("color", "red" );
                                selectedElement.parent().find('.wrong').removeClass('hidden');
                               $('.modal-footer p').removeClass('hidden');
                           }
                       } else if(parseInt(answer) + 1 === this.state.exam.questions.length ) {
                                       $('.congratulations').removeClass('hidden');
                                       $('finish-question').removeClass('hidden');
                                       $('.next-question').addClass('hidden');
                                       $(e.target).addClass('hidden');
                       }
                    });
            }else {
                swal(
                        'No Quiz created',
                        "Sorry we couldn't find any quiz for this module",
                        'error'
                      )
            }
       });
     
  }
    componentDidMount() {
        let alias  = location.pathname.split('/').pop();
        $.get("/api/catalog/courses/curriculum", {alias: alias, limit: 1}, (res) => {
            if(!res.data.curriculum){
                res.data.curriculum = [];
            }
            
             if(!res.data.resources){
                res.data.resources = [];
            }
            
            if(res.data.curriculum.length > 0) {
                if (Array.isArray(res.data.curriculum[0].curriculum)){
                    let module = res.data.curriculum[0];
                    let current = {};
                    if(Array.isArray(module.curriculum)){
                        current = module.curriculum[0];
                    }
                    this.setState({course: res.data, module : module, current: current});
                }else {
                     this.setState({course: res.data});
                }
            }else {
                this.setState({course: res.data});
            }
            
            $('.module-click').off().on('click',(e)=>{
                let parent = $(e.target).data('parent');
                let index = $(e.target).attr('id');
                if (Array.isArray(res.data.curriculum[parent].curriculum)){
                    let module = res.data.curriculum[parent];
                    let current =  module.curriculum[index];
                   if(current.is_quiz === 'Y'){
                      this.loadExam(current);
                   }else {
                    this.setState({course: res.data, module : module, current: current,curriculumIndex : index, currentModuleIndex: parent});
                }
                }
                
            });
        });
    }
    render() {
       
        let display = null;
        
        if(this.state.isText === true ) {
            display = <CurrentText module={ this.state.current } />
        } else {
           display = <CurrentVideo module={ this.state.current } />
        }
        return (
                <div className="row">
                   
                    <section className="inner-banner pattern-3">
                    <div className="container text-center">
                        <h2>{ this.state.course.title }</h2>
                        <p>{ this.state.course.description }</p>
                    </div>
                    </section>
                     <div className="container">
                    <div className="col-sm-4 read-sidebar hidden-xs">
                         <h3 className="read-outline-hdr">Course Outline</h3>
                        <div className="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        { this.state.course.curriculum.map( (curriculum, idx) => <LoadTabs curriculum={ curriculum } key={ idx } id={ idx }  /> )}
                        </div>
                    </div>
                   <div className="col-sm-8 read-container">
                    <ol className="breadcrumb read-breadcrumb">
                        <li><a href="/my/account">My Account</a></li>
                        <li><a href="/my/course">my Courses</a></li>
                        <li className="active">{ this.state.course.title }</li>
                    </ol>
                      <div className="row course-material">
                    { display }
            </div>
            <div className="row">
               
                <div className="col-sm-12 clearfix mt10 button-container">
                    <div className="pull-left"><a href="#" id="view-modules" className="btn btn-default btn-md" role="button" onClick={ this.handleSwitchView } >View Video</a></div>
                            <div className="pull-right"><a href="#" id="next-modules" className="btn btn-success btn-md" role="button" onClick={ this.handleNext } >Next</a></div>
                </div>
                <div className="col-sm-12 mb30">
                 <div className="row">
                    <div className="col-xs-12 current-module">Current { this.state.module.title }</div>
                <div className="col-xs-12 module-listing">
                    <div className="row module-listing-row">
                    { this.state.module.curriculum.map( (module, idx) => ( <div className="col-xs-2 module-listing-middle module-click" data-parent={ this.state.currentModuleIndex } key={idx} id={ idx }>{ module.title } <i className="fa fa-angle-double-right"></i></div> ) )}
                        
                    </div>
                </div>
                </div>
                </div>
            </div>
                </div>
                </div>
                <LoadExam exam={ this.state.exam } />
                </div>
                            );
                }

    }
    
    class CurrentText extends React.Component{
        render(){
            return(
                    <div className="col-sm-12"><div className="embed-responsive embed-responsive-4by3">
                <iframe className="embed-responsive-item" src={ this.props.module.read } frameBorder="0" allowFullScreen="allowfullScreen"></iframe>
                </div></div>    
                )
        }
    }
    
    class CurrentVideo extends React.Component{
        render(){
            return(
                <div className="col-sm-12"><div className="embed-responsive embed-responsive-4by3">
                <iframe  className="embed-responsive-item"  src={ this.props.module.video } frameBorder="0" allowFullScreen="allowfullScreen"></iframe>
                </div></div>  
                )
        }
    }
  
  
  
  class LoadQuestion extends React.Component {
   render(){
       let question = this.props.question;
       this.active = 0
       let answers = [];
       for(let a in question.answers ) {
           answers.push(question.answers[a]);
       }
       let hidden = 'hidden';
       if(parseInt(this.props.id) == this.active ) {
           hidden = ' active ';
       }
       
        return ( 
                <li className={ 'card-item ' + hidden} data-number={'question' + parseInt(this.props.id) + 1}  > <span className="qnumber">{parseInt(this.props.id) + 1}.</span>{ question.title} 
                        { answers.map((answer, idx) => (
                                
                        <div className="row answers-listing" key={idx} id={ idx } data-id={ idx  }>  
                        <div className="col-xs-1 numbering-style"><span>{answer.numbering_style }</span></div>
                        <div className="col-xs-11 col-sm-11">
                                <div className="radio">
                                    <label> 
                                      <input type="radio" name={ 'number' + this.props.id }   data-id={ idx  } data-parent={ this.props.id } />
                                      { answer.title } <i className="fa fa-times wrong hidden"></i> <i className="fa fa-check correct hidden"></i>
                                    </label>
                                  </div>
                                  </div>
                            </div>
                            
                         ))}
                 </li>
              )
    }
}

 class LoadExam extends React.Component {
   
    render(){
        let exam = this.props.exam || {};
        if(!Array.isArray(exam.questions)){
            exam.questions = [];
        }
        
       
        return (
            <div className="modal fade" id="questions-modal" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div className="modal-dialog modal-lg" role="document">
                      <div className="modal-content">
                        <div className="modal-header">
                            <div className="header">
                          <ul className="list">
                              <li className="item item-active"><p><span className="number">1</span> <span className="text">Start</span></p></li>
                          <li className="item"><p><span className="number">{ exam.questions.length }</span> <span className="text">Finish</span></p></li>
                          </ul>
                            </div>
                            </div>
                        <div className="modal-body">
                            <ol className="card">
                               {  exam.questions.map( (question, idx) => <LoadQuestion question={ question } key={ idx } id={ idx }  /> )}
                            </ol>
                            <div className="hidden congratulations"><h1>Congratulations!!</h1> <p>You have completed your quiz for this module </p></div>
                        </div>
                        <div className="modal-footer">
                        <p className="pull-left hidden">Incorrect</p>
                          <button type="button" className="btn btn-black verify-answer" >Verify Answer</button><button type="button" className="btn btn-black next-question hidden">NEXT</button> <button type="button" className="btn btn-success finish-question hidden" data-dismiss="modal">FINISH</button>
                        </div>
                      </div>
                    </div>
                  </div>
        )
    }
 }

class LoadTabs extends React.Component {
    render() {
        let tab = this.props.curriculum;
        let modules = [];
        if (Array.isArray(tab.curriculum)){
            modules = tab.curriculum;
        }
       return ( 
                <div className="panel panel-default">
                           <div className="panel-heading" role="tab" id={ 'tab-' + this.props.id } >
                               <h4 className="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion" href={ '#collapse' + this.props.id } aria-expanded="true" aria-controls={ 'collapse' + this.props.id }> { tab.title } </a></h4>
                           </div>
                           <div id="collapseOne" className="panel-collapse collapse in" role="tabpanel" aria-labelledby={ 'heading'  + this.props.id } >
                               <div className="panel-body">
                                   <div className="list-group">
                                    { modules.map((sub, idx) => (
                                        <button type="button" className="list-group-item module-click" data-parent={ this.props.id } key={idx} id={ idx }>{ sub.title }</button>
                                    ))}
                                   </div>
                               </div>
                           </div>
                       </div>
                
       )
                
    }
}
    export default CourseRead;
    if ($('#course-view-page').length > 0) {
        render(<CourseRead />, document.getElementById('course-view-page'));
    }

