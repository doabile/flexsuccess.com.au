import React from 'react';
import {render} from 'react-dom';

class BlogsByCategory extends React.Component {
    constructor(props) {
        super(props);
        this.state = {blogs : []};
    }

    componentDidMount() {
       let alias = location.pathname.split('/')[3];
       if(alias) {
        $.get("/api/cms/blogs/blogsbycategory", {limit : 30, alias : alias}, (result) => {
            if(result.success === true) {
                this.setState({blogs: result.data});
                $('.dynamic-title').text(result.category.name);
            }
        });
    }
    }
    render() {
        return (
                
               <div className="single-sidebar post-widget">
                    <ul className="recent-news">
                     { this.state.blogs.map( (blog, idx) => <BlogCategoryList blog={ blog } key={ idx } /> )}  
                     </ul>
               </div>
                      
        )
    }
    
}
class BlogCategoryList extends React.Component {
   truncate(str, max = 10) {
        if (str) {
            let strArr = str.toString().split(" ");
            if (strArr.length > max) {
                return strArr.splice(0, max).join(" ") + '...';
            }else {
                return str;
            }
        }
        return " ";
    }
    
    render() {
        const blog = this.props.blog;
        const content = this.truncate(blog.intro, 30);
        return (
                       <li>
                    <div className="box search-list">
                      <img alt={ blog.title } src={ blog.image_path } />
                       
                    </div>
                   <div className="content">
                   <h3><a  href={ '/blogs/' + blog.alias }>{ blog.title }</a></h3>
                         <div dangerouslySetInnerHTML={{__html: content }} ></div>
                         <p className="text-right"> <a className="read-more arrow-push-in-right" href={ '/blogs/' + blog.alias }><span>read blog <i className="fa fa-caret-right"></i></span></a></p>
                   
                    </div>
                   
                     
                </li>
                    
                );
    }
}

export default BlogsByCategory;
if($('#search-blogs-by-category').length > 0 ) {
render( <BlogsByCategory />, document.getElementById('search-blogs-by-category'));
}

