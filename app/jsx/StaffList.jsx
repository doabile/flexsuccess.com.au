import React from 'react';
import {render} from 'react-dom';

class StaffList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {staffList : []};
    }

    componentDidMount() {
        $.get("/api/crm/staff/list", { limit:20 },(data) => {
            this.setState({staffList: data.data});
            $(function () {
                setTimeout(()=>{
                     $('.staff-list').owlCarousel({
                loop: true,
                nav: true,
                navText: [
                    '<i class="fa fa-angle-left"></i>',
                    '<i class="fa fa-angle-right"></i>'
                ],
                margin: 50,
                autoplay: true,
                autoplayTimeout: 3000,
                autoplayHoverPause: true,
                dots: false,
               //  autoWidth: true,
                responsive: {
                    0: {
                        items: 1
                    },
                     480: {
                        items: 1
                    },
                    600: {
                        items: 3
                    },
                    1000: {
                        items: 5
                    }
                }
        });
                }, 1000)
        });
    });
    }
    render() {
        return (
                    
                     <section className="experienced-trainer-area">
                        <div className="container">
                            <div className="section-title text-center">
                                <h1><span>Our experienced Coaches</span></h1>
                            </div>
                            <div className="staff-list">
                        			{ this.state.staffList.map( (staff, idx) => <StaffView staff={ staff } key={ idx } /> )}
                        	</div>
                        </div>
                        </section>
                  
                );
    }

}

class StaffView extends React.Component {
    render() {
        const staff = this.props.staff;
        const name = staff.firstname ;
        return (
                 
                      <div className="single-item single-popular-class">
                          <div className="img-box"><span className="thumbnail"><img alt="" src={ staff.gravatar} alt={ staff.firstname } /></span>
                              <div className="overlay">
                                  <div className="content">
                                      <div className="box text-center"><h3><a href={'/coaches#' +  staff.firstname.trim().toLowerCase() }>More</a></h3></div>
                                  </div>
                              </div>
                          </div>

                          <div className="content" style={{paddingRight:0}}>
                              <div className="trainer-name">
                                  <div className="name">
                                      <h3 className="staff-name-h3"><a href={'/coaches#' +  staff.firstname.trim().toLowerCase() }>{ name }</a> <div className="red pull-right"> <i className={ staff.custom }></i></div></h3>
                                      <div className="clearfix"></div>  
                                      <span>{ staff.jobtitle }</span></div>
                              </div>
                          </div>
                      </div>
                );
    }
}
export default StaffList;
if($('#home-staff-list').length > 0 ) {
render( <StaffList />, document.getElementById('home-staff-list'));
}
