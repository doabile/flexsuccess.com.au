import React from 'react';
import {render} from 'react-dom';

class FeaturedFeeds extends React.Component {
    constructor(props) {
        super(props);
        this.state = {blogs: [], recipes : []};
    }

    componentDidMount() {
        $.get("/api/cms/blogs/feed",{limit : 2, include : 'recipe'}, (res) => {
            this.setState({blogs: res.blogs, recipes: res.recipes });
        });
    }
    render() {
        return (
                  <div className="col-sm-12">
                      <div className="section-title text-left">
                      <h1 className="text-center"><span>Featured Reads</span></h1>
                      </div>
                        <div className="row">
                           {this.state.blogs.map((blog, idx) => <BlogFeed blog={ blog } key={ idx } />)}
                           {this.state.recipes.map((recipe, idx) => <RecipeFeed recipe={ recipe } key={ idx } />)}
                         </div>
                  </div>
                );
    }

}

class BlogFeed extends React.Component {
    render() {
        const blog = this.props.blog;
        return (
                <div className="col-sm-3">
                    <div className="single-popular-class">
                        <div className="img-box"><a href={'/blogs/' + blog.alias } className="featured-product"><img alt={ blog.title } src={ blog.image_path } /></a>
                            <div className="overlay">
                                <div className="content">
                                    <div className="box text-center"><h3><a href={'/blogs/' + blog.alias}>View</a></h3></div>
                                   </div>
                                </div>
                            </div>
                        <div className="text-box clearfix">
                            <div className="left-box pull-left">
                                <h4><a href={'/blogs/' + blog.alias }>{ blog.title }</a></h4>
                            </div>
                        </div>
                    </div>
                </div>
                );
    }
}

class RecipeFeed extends React.Component {
    render() {
        const recipe = this.props.recipe;
        return (
                <div className="col-sm-3">
                    <div className="single-popular-class">
                        <div className="img-box"><a href={'/recipes/' + recipe.alias } className="featured-product"><img alt={ recipe.title } src={ recipe.image_path } /></a>
                            <div className="overlay">
                                <div className="content">
                                    <div className="box text-center"><h3><a href={'/recipes/' + recipe.alias}>View</a></h3></div>
                                   </div>
                                </div>
                            </div>
                        <div className="text-box clearfix">
                            <div className="left-box pull-left">
                                <h4><a href={'/recipes/' + recipe.alias }>{ recipe.title }</a></h4>
                            </div>
                        </div>
                    </div>
                </div>
                );
    }
}

export default FeaturedFeeds;
if($('#featured-reads').length > 0 ) {
render(<FeaturedFeeds />, document.getElementById('featured-reads'));
}
