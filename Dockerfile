FROM mhart/alpine-node:8
RUN apk update && \
    apk upgrade

LABEL au.com.bbdiet.version="0.0.1-beta"
LABEL vendor="ERPSYNC PTY LTD"

RUN mkdir -p /var/www
WORKDIR /var/www

# If you have native dependencies, you'll need extra tools
# RUN apk add --no-cache make gcc g++ python
RUN apk add libc6-compat
RUN apk add joe

#COPY package.json /var/www/html
# If you need npm, don't use a base tag
#COPY package.json /var/www
#COPY app.js /var/www
ADD . .
RUN npm install
RUN npm install forever --global

EXPOSE 80

CMD ["forever", "app.js", "80"]
#CMD [ "npm", "start" ]
