var webpack = require('webpack');
var path = require('path');

var BUILD_DIR = path.resolve(__dirname, 'app/public');
var APP_DIR = path.resolve(__dirname, 'app/jsx');

var config = {
  entry: APP_DIR + '/index.jsx',
// entry: APP_DIR + '/my.jsx',
   plugins: [
  new webpack.DefinePlugin({
    'process.env': {
      'NODE_ENV': JSON.stringify('production')
    }
  }),
    new webpack.optimize.UglifyJsPlugin({
      compress: { warnings: false },
      mangle: true,
      sourcemap: false,
      beautify: false,
      dead_code: true
    })
],

  output: {
    path: BUILD_DIR,
    filename: 'bundle.js'
  //  filename: 'my.js'
  },
    module : {
    loaders : [
      {
        test : /\.jsx?/,
        include : APP_DIR,
        loader : 'babel-loader'
      },
      {
      test: /\.(png|jpg|gif)$/,
      loader: 'url?limit=25000'
    }
    ]
  }
};

module.exports = config;